#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash firefox dmenu zenity

# Runs firefox, in a jail, using dmenu to select a profile
# By Brian Cole

set -x
set -e

PPATH="$HOME/.mozilla/firefox"
cd "$PPATH"

PROFILE="$(find . -maxdepth 1 ! -name . ! -name 'Pending Pings' ! -name 'Crash Reports' | sed 's/^\.\///' | sort -f | dmenu -i)"

# base command: invoke firejail and tell it we're running Firefox
COMMAND="firejail"

# now on to firefox parameters
COMMAND="$COMMAND --"

# keep sessions separate
COMMAND="$COMMAND firefox --no-remote"

echo "Looking at $PROFILE"
if test "$PROFILE" = 'profiles.ini'
then
	echo Selected profiles.ini, assuming you want to make a new profile
	PROFILE="$(zenity --entry --text='New profile name?')"
	# TODO: This could use a template profile
	mkdir "$PPATH/$PROFILE"
fi

COMMAND="$COMMAND --profile $PPATH/$PROFILE"

# echo and run the final command
echo "We should now run:"
echo "$COMMAND"
$COMMAND

# ff

The ff script is a wrapper for Firefox. It runs firefox in a jail using firejail, and uses dmenu to select a profile (not pretty, but functional enough; I mostly wanted to type to select).


## Source

Primary: https://git.sr.ht/~yjftsjthsd/ff
Secondary: https://gitlab.com/yjftsjthsd-g/ff


## Dependencies

* Bourne shell (BASH is fine)
* dmenu (https://tools.suckless.org/dmenu/)
* Firejail (https://firejail.wordpress.com/)
* Zenity (https://gitlab.gnome.org/GNOME/zenity)


## Use

Just run the ff script (`./ff`) or copy it to somewhere in your $PATH and run it
from there.

You should be presented with a dmenu list of profiles; type to fiter and hit
enter when the correct profile is selected. To create a new profile, select the
"profiles.ini" option (ugly hack, but you're never going to use that option as
an actual "profile" and I needed a way to make a new profile on the fly...) then
put the new profile name in the next dialog.


## Variants

Also included in this repo are `ff-nix` and `ff-nojail`; these are modified
versions that respectively support NixOS and platforms without firejail (such as
the BSD family). Note that without firejail, you lose the security benefits and
ff is only useful for using multiple profiles.


## Packaging


### Arch

The included Dockerfile will build an Arch package using FPM.
```
docker build . -t ff-build
docker run -v $PWD:/opt/ff ff-build
```
This will create an output file in the current directory like
`ff-1.4-3-any.pkg.tar.xz` for you to install.


### Nix

```
nix-build -A ff
nix-env -i ./result
```


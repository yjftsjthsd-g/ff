rec {
  pkgs = import <nixpkgs> {};
  ff = pkgs.stdenv.mkDerivation rec {
    name = "ff";
    builder = builtins.toFile "builder.sh" ''
      source $stdenv/setup
      mkdir -p $out/bin
      substituteAll "${./ff-nix}" "$out/bin/ff"
      chmod a+x "$out/bin/ff"
    '';
  };
}

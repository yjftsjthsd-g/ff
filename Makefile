

debian: ff_1.5-0_any.deb

ff_1.5-0_any.deb: out/usr/bin/ff out/DEBIAN/control
	dpkg-deb --build --root-owner-group out ff_1.5-0_any.deb

out/usr/bin/ff: ff
	mkdir -p out/usr/bin
	cp ./ff out/usr/bin/ff

out/DEBIAN/control: DEBIAN/control
	mkdir -p out/DEBIAN
	cp ./DEBIAN/control out/DEBIAN/control


clean:
	rm -rf ff_1.5-0_any.deb out




# Changelog

## future
merges ff variants

## version 1.5, 2020-02-28
filter out "Crash Reports" and "Pending Pings"

## version 1.4.1, 2020-02-28
shellcheck fixes

## version 1.4, 2016-06-04
correctly autodetects firefox location if on PATH

## version 1.3
merges 1.2 and 1.2b, providing a cleaner version with all features yet supported

## version 1.2b, 2016-03-15
add variable for firefox binary

## version 1.2, 2016-03-01
allows creation of new profile with zenity (avoids official interface to let us set directory name, and it's nicer this way)

## version 1.1, 2016-02-17
fixes bugs and sorts profiles (case-insensitive)

## version 1.0, 2016-01-16
earliest record, not actually versioned at the time

